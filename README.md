# Tobacco Maps

## Setting up

### Fetching source code

There are two ways to fetch the source code. The preferred way is to do via git, but requires a git client installed. The alternative is to download zip and extract contents.

#### Via git

To fetch the source code, [install git](https://git-scm.com/downloads) and fetch the project from [gitlab.com/asdofindia/tobacco-maps](https://gitlab.com/asdofindia/tobacco-maps/).

* `git clone --recursive https://gitlab.com/asdofindia/tobacco-maps.git`

#### Downloading zip (alternative)

On the [project page](https://gitlab.com/asdofindia/tobacco-maps/) there is a download button which gives option to download as zip file. [Direct link](https://gitlab.com/asdofindia/tobacco-maps/-/archive/main/tobacco-maps-main.zip)

With this there is an extra step to download the maps. The maps are in [github.com/asdofindia/maps/tree/old-states](https://github.com/asdofindia/maps/tree/old-states) and can be downloaded through the clone or download button. [Direct link](https://github.com/asdofindia/maps/archive/old-states.zip)

After unzipping both, the `maps` folder has to be inside `tobacco-maps` folder like this:

```
| - tobacco-maps
  | - maps
  | - README.md
  | - charts.R
  | - common.R
  | - maps.R
  | - tobacco.Rproj
```

### Data

No data is kept in the source code. Data is read from external files. But for the code to know where those files are, a configuration has to be made.

You will need a file named `.env` with the paths to data files like this:

```ini
DATA_FILE="~/data/tobacco-rates.xlsx"
LOCATION_DATA_FILE="~/data/location.csv"
SOCIAL_DATA_FILE="~/data/social.csv"
MPCE_DATA_FILE="~/data/mpce.csv"
```

### Rstudio or R

To run the project, an installation of [RStudio](https://rstudio.com/) may be required. One can directly use [R](https://www.r-project.org/) as well.

If using RStudio, open the `tobacco.Rproj` in it.

There are many libraries that are dependencies required to run. One could install all of them globally with this command:

```r
install.packages(c("sf", "ggplot2", "rgeos", "rgdal", "maptools", "gridExtra", "dplyr", "dotenv", "tidyverse", "sf", "ggplot2", "readxl", "reshape2", "ggthemes"))
```

To actually run the code in Rstudio, one has to open the `maps.R` file or `charts.R` file and press `"Source"`. This will respectively create the maps or the charts.


## Code overview

Any functionality related to mapping is in `maps.R`

For example, if one needs to change the color of the maps, they can do so by editing the `plot_map` function (or the `plot_change` for maps that show change)
